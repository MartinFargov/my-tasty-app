package com.map7y7o.marto.tastyapp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

/**
 * This is the starting screen of the App
 */
public class MainActivity extends AppCompatActivity {

    private TextView mTextMessage;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    return setTextViewText(R.string.title_home);
                case R.id.navigation_dashboard:
                    return setTextViewText(R.string.title_dashboard);
                case R.id.navigation_notifications:
                    return setTextViewText(R.string.title_notifications);
            }
            return false;
        }
    };

    /**
     * Sets the passed res ID text to the mTextMessage text view
     * @param resId - String res ID
     * @return true
     */
    boolean setTextViewText(int resId){
        mTextMessage.setText(resId);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

}
